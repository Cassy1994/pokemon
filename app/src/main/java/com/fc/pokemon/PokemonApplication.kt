package com.fc.pokemon

import android.app.Application
import com.fc.cache.di.cacheModule
import com.fc.network.di.networkModule
import com.fc.pokemon.di.presentationModule
import com.fc.repository.di.pokemonRepositoryModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class PokemonApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            val modules =
                listOf(cacheModule, networkModule, pokemonRepositoryModule, presentationModule)

            androidLogger(Level.NONE)
            androidContext(this@PokemonApplication)
            modules(modules)
        }
    }
}