package com.fc.pokemon.di

import com.fc.domain.*
import com.fc.pokemon.ui.PokemonsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule = module {

    single {
        GetPokemonsUseCase(pokemonsRepository = get())
    }

    single {
        SearchPokemonsUseCase(pokemonsRepository = get())
    }

    single {
        AddRemoveFavoritePokemonUseCase(pokemonsRepository = get())
    }

    single {
        GetFavoritesPokemonsUseCase(pokemonsRepository = get())
    }

    single {
        GetPokemonDetailsUseCase(pokemonsRepository = get())
    }

    viewModel { PokemonsViewModel(get(), get(), get(), get(), get()) }
}
