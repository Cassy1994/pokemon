package com.fc.pokemon.utils

import androidx.lifecycle.MutableLiveData

interface LoadingAndErrorHandler {

    val errorsLiveData: MutableLiveData<List<String>>
    fun getErrors() =  errorsLiveData

    val loadingLiveData: MutableLiveData<Boolean>
    fun isLoading() =  loadingLiveData
}

fun LoadingAndErrorHandler(): LoadingAndErrorHandler = object : LoadingAndErrorHandler {
    override val errorsLiveData = MutableLiveData<List<String>>()
    override val loadingLiveData = MutableLiveData<Boolean>()
}