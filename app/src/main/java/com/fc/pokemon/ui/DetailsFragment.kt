package com.fc.pokemon.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.fc.domain.models.details.Ability
import com.fc.pokemon.R
import com.fc.pokemon.databinding.FragmentDetailsBinding
import com.fc.pokemon.ui.adapter.AbilitiesAdapter
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class DetailsFragment : Fragment() {

    private val pokemonsViewModel: PokemonsViewModel by sharedViewModel()
    private var _binding: FragmentDetailsBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailsBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val bundle = arguments

        if (bundle == null) {
            Log.e("Error", "DetailsFragment did not receive pokemon url")
        } else {
            val args = DetailsFragmentArgs.fromBundle(bundle)
            pokemonsViewModel.pokemonDetails.observe(viewLifecycleOwner) {
                initRecyclerView(it.abilities)
            }

            Glide
                .with(requireContext())
                .load(args.photoUrl)
                .centerCrop()
                .placeholder(R.drawable.ic_launcher_foreground)
                .into(binding.pokemonPhoto)

            binding.pokemonNameDetails.text = args.name

            pokemonsViewModel.getPokemon(args.url)
        }

        return root
    }

    private fun initRecyclerView(abilities: List<Ability>) {
        with(binding) {
            abilitiesRecyclerview.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            abilitiesRecyclerview.adapter = AbilitiesAdapter(abilities)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}