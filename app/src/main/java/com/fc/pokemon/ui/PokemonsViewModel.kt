package com.fc.pokemon.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fc.domain.*
import com.fc.domain.models.Pokemon
import com.fc.domain.models.Resource
import com.fc.domain.models.details.PokemonDetails
import com.fc.domain.utils.Const.START_OFFSET
import com.fc.pokemon.utils.LoadingAndErrorHandler
import kotlinx.coroutines.launch

class PokemonsViewModel constructor(
    private val getPokemonsUseCase: GetPokemonsUseCase,
    private val searchPokemonsUseCase: SearchPokemonsUseCase,
    private val insertFavoritePokemon: AddRemoveFavoritePokemonUseCase,
    private val getFavoritesPokemonsUseCase: GetFavoritesPokemonsUseCase,
    private val getPokemonDetailsUseCase: GetPokemonDetailsUseCase
) : ViewModel(), LoadingAndErrorHandler by LoadingAndErrorHandler() {

    private val _pokemons = MutableLiveData<List<Pokemon>>()
    val pokemons: LiveData<List<Pokemon>> get() = _pokemons

    private val _researchPokemons = MutableLiveData<List<Pokemon>>()
    val researchPokemons: LiveData<List<Pokemon>> get() = _researchPokemons

    private val _favoritePokemon = MutableLiveData<Pokemon>()
    val favoritePokemon: LiveData<Pokemon> get() = _favoritePokemon

    private val _favoritesPokemon = MutableLiveData<List<Pokemon>>()
    val favoritesPokemon: LiveData<List<Pokemon>> get() = _favoritesPokemon

    private val _pokemonDetails = MutableLiveData<PokemonDetails>()
    val pokemonDetails: LiveData<PokemonDetails> get() = _pokemonDetails

    var offset = 0

    init {
        getPokemons()
    }

    fun getPokemons(offset: Int = START_OFFSET) {
        viewModelScope.launch {
            getPokemonsUseCase(offset).collect { result ->
                when (result) {
                    is Resource.Success -> {
                        loadingLiveData.value = false
                        _pokemons.value = result.data ?: listOf()
                    }
                    is Resource.Error -> {
                        loadingLiveData.value = false
                        errorsLiveData.value = listOf(result.throwable.message ?: "Generic Error")
                    }
                    is Resource.Loading -> {
                        loadingLiveData.value = true
                    }
                }
            }
        }
    }

    fun searchPokemon(query: CharSequence) {
        if (query.isBlank()) getPokemons()
        viewModelScope.launch {
            searchPokemonsUseCase(query).collect { result ->
                when (result) {
                    is Resource.Success -> {
                        _researchPokemons.value = result.data ?: listOf()
                    }
                    is Resource.Error -> {
                        errorsLiveData.value = listOf(result.throwable.message ?: "Generic Error")
                    }
                    is Resource.Loading -> { }
                }
            }
        }
    }

    fun addRemoveFavoritePokemon(pokemon: Pokemon) {
        viewModelScope.launch {
            insertFavoritePokemon(pokemon).collect { result ->
                when (result) {
                    is Resource.Success -> {
                        result.data.let { _favoritePokemon.value = it }
                    }
                    is Resource.Error -> {
                        errorsLiveData.value = listOf(result.throwable.message ?: "Generic Error")
                    }
                    is Resource.Loading -> { }
                }
            }
        }
    }

    fun getFavoritesPokemons() {
        viewModelScope.launch {
            getFavoritesPokemonsUseCase().collect { result ->
                when (result) {
                    is Resource.Success -> {
                        loadingLiveData.value = false
                        _favoritesPokemon.value = result.data ?: listOf()
                    }
                    is Resource.Error -> {
                        loadingLiveData.value = false
                        errorsLiveData.value = listOf(result.throwable.message ?: "Generic Error")
                    }
                    is Resource.Loading -> {
                        loadingLiveData.value = true
                    }
                }
            }
        }
    }

    fun getPokemon(url: String) {
        viewModelScope.launch {
            getPokemonDetailsUseCase(url).collect { result ->
                when (result) {
                    is Resource.Success -> {
                        loadingLiveData.value = false
                        result.data.let { _pokemonDetails.value = it }
                    }
                    is Resource.Error -> {
                        loadingLiveData.value = false
                        errorsLiveData.value = listOf(result.throwable.message ?: "Generic Error")
                    }
                    is Resource.Loading -> {
                        loadingLiveData.value = true
                    }
                }
            }
        }
    }
}