package com.fc.pokemon.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fc.domain.models.Pokemon
import com.fc.pokemon.R
import com.fc.pokemon.databinding.PokemonRowBinding

class PokemonsAdapter(
    private val onClickFavoriteBtn: (Pokemon) -> Unit,
    private val onClickDetails: (Pokemon) -> Unit
) : RecyclerView.Adapter<PokemonsAdapter.PokemonViewHolder>() {

    private var pokemons: MutableList<Pokemon> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonViewHolder {
        return PokemonViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.pokemon_row, parent, false),
            onClickFavoriteBtn,
            onClickDetails
        )
    }

    override fun onBindViewHolder(holder: PokemonViewHolder, position: Int) =
        holder.bindPresentation(pokemons[position], position)

    override fun getItemCount(): Int {
        return pokemons.size
    }

    class PokemonViewHolder(
        itemView: View,
        val onClickBtn: (Pokemon) -> Unit,
        val onClickDetails: (Pokemon) -> Unit
    ) : RecyclerView.ViewHolder(itemView) {

        private val ctx: Context = itemView.context
        private val binding = PokemonRowBinding.bind(itemView)

        fun bindPresentation(pokemon: Pokemon, position: Int) {
            itemView.setOnClickListener { onClickDetails(pokemon) }
            with(binding) {
                pokemonName.text = pokemon.name
                pokemonNr.text = ctx.getString(R.string.pokemon_nr, position)
                favoriteBtn.apply {
                    setOnClickListener { onClickBtn(pokemon) }
                    setImageResource(if (pokemon.isFavorite) R.drawable.ic_heart else R.drawable.ic_empty_hearth)
                }
                Glide
                    .with(itemView)
                    .load(pokemon.photoUrl)
                    .centerCrop()
                    .placeholder(R.drawable.ic_launcher_foreground)
                    .into(photo)
            }
        }
    }

    fun appendNewItems(oldTotalCount: Int = pokemons.size, newPokemons: List<Pokemon>) {
        this.pokemons.addAll(newPokemons)
        notifyItemMoved(oldTotalCount, pokemons.size)
    }

    fun researchResults(pokemons: List<Pokemon>) {
        if (pokemons.isEmpty()) return
        this.pokemons = pokemons.toMutableList()
        notifyDataSetChanged()
    }

    fun updateItem(pokemon: Pokemon) {
        pokemons.mapIndexed { index, it ->
            if (it.name == pokemon.name) {
                val newPokemon = it.copy(isFavorite = pokemon.isFavorite)
                pokemons[index] = newPokemon
                notifyItemChanged(index)
            }
        }
    }

    abstract class OnScrollListener: RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            if (dy > 0) {
                val lastVisibleItemPosition = (recyclerView.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
                val totalItem = recyclerView.adapter?.itemCount ?: run { 0 }
                if (lastVisibleItemPosition >= (totalItem.minus(1))) {
                    loadMore()
                }
            }
        }

        abstract fun loadMore()
    }
}