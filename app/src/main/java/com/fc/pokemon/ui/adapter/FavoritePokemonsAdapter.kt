package com.fc.pokemon.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fc.domain.models.Pokemon
import com.fc.pokemon.R
import com.fc.pokemon.databinding.FavoritePokemonRowBinding

class FavoritePokemonsAdapter(
    private val pokemons: List<Pokemon>,
    private val onClickDetails: (Pokemon) -> Unit
) :
    RecyclerView.Adapter<FavoritePokemonsAdapter.PokemonViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonViewHolder {
        return PokemonViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.favorite_pokemon_row, parent, false),
            onClickDetails
        )
    }

    override fun onBindViewHolder(holder: PokemonViewHolder, position: Int) =
        holder.bindPresentation(pokemons[position], position)

    override fun getItemCount(): Int {
        return pokemons.size
    }

    class PokemonViewHolder(itemView: View, private val onClickDetails: (Pokemon) -> Unit) :
        RecyclerView.ViewHolder(itemView) {
        private val ctx: Context = itemView.context
        private val binding = FavoritePokemonRowBinding.bind(itemView)

        fun bindPresentation(pokemon: Pokemon, position: Int) {
            itemView.setOnClickListener { onClickDetails(pokemon) }
            with(binding) {
                pokemonName.text = pokemon.name
                pokemonNr.text = ctx.getString(R.string.pokemon_nr, position)
                Glide
                    .with(itemView)
                    .load(pokemon.photoUrl)
                    .centerCrop()
                    .placeholder(R.drawable.ic_launcher_foreground)
                    .into(photo)
            }
        }
    }
}