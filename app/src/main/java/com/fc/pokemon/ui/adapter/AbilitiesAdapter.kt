package com.fc.pokemon.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fc.domain.models.details.Ability
import com.fc.pokemon.R
import com.fc.pokemon.databinding.AbilityRowBinding
import kotlin.random.Random

class AbilitiesAdapter(private val abilities: List<Ability>) :
    RecyclerView.Adapter<AbilitiesAdapter.AbilitiesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbilitiesViewHolder {
        return AbilitiesViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.ability_row, parent, false)
        )
    }

    override fun onBindViewHolder(holder: AbilitiesViewHolder, position: Int) =
        holder.bindPresentation(abilities[position])

    override fun getItemCount(): Int {
        return abilities.size
    }

    class AbilitiesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding = AbilityRowBinding.bind(itemView)

        fun bindPresentation(ability: Ability) {
            with(binding) {
                abilityName.text = ability.ability.name
                abilityProgressBar.progress = Random.nextInt(0, 100)
            }
        }
    }
}