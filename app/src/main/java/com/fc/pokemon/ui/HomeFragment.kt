package com.fc.pokemon.ui

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.fc.domain.models.Pokemon
import com.fc.domain.utils.Const.LIMIT
import com.fc.pokemon.databinding.FragmentHomeBinding
import com.fc.pokemon.ui.adapter.PokemonsAdapter
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class HomeFragment : Fragment() {

    private val pokemonsViewModel: PokemonsViewModel by sharedViewModel()
    private var _binding: FragmentHomeBinding? = null

    private val binding get() = _binding!!

    private val pokemonsAdapter by lazy {
        PokemonsAdapter(
            ::onClickFavorite,
            ::onClickDetails
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.searchPokemon.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s?.let { pokemonsViewModel.searchPokemon(it) }
            }
        })

        initRecyclerView()

        pokemonsViewModel.pokemons.observe(viewLifecycleOwner) { pokemons ->
            pokemonsAdapter.appendNewItems(newPokemons = pokemons)
        }

        pokemonsViewModel.researchPokemons.observe(viewLifecycleOwner) { pokemons ->
            pokemonsAdapter.researchResults(pokemons = pokemons)
        }

        pokemonsViewModel.favoritePokemon.observe(viewLifecycleOwner) {
            pokemonsAdapter.updateItem(it)
        }

        pokemonsViewModel.getErrors().observe(viewLifecycleOwner) {
            Toast.makeText(context, it.firstOrNull(), Toast.LENGTH_SHORT).show()
        }

        pokemonsViewModel.isLoading().observe(viewLifecycleOwner) {
            binding.loadingProgressBar.isVisible = it
        }

        return root
    }

    private fun initRecyclerView() {
        with(binding) {
            binding.pokemonsRecyclerview.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            binding.pokemonsRecyclerview.adapter = pokemonsAdapter
            pokemonsRecyclerview.addOnScrollListener(object : PokemonsAdapter.OnScrollListener() {
                override fun loadMore() {
                    pokemonsViewModel.getPokemons(offset = pokemonsViewModel.offset + LIMIT)
                }
            })
        }
    }

    private fun onClickFavorite(pokemon: Pokemon) =
        pokemonsViewModel.addRemoveFavoritePokemon(pokemon.copy(isFavorite = !pokemon.isFavorite))

    private fun onClickDetails(pokemon: Pokemon) {
        val action = HomeFragmentDirections.actionHomeFragmentToDetailsFragment(
            pokemon.url,
            pokemon.name,
            pokemon.photoUrl
        )
        findNavController().navigate(action)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}