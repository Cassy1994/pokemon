package com.fc.pokemon.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.fc.domain.models.Pokemon
import com.fc.pokemon.databinding.FragmentFavoritesBinding
import com.fc.pokemon.ui.adapter.FavoritePokemonsAdapter
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class FavoritesFragment : Fragment() {

    private val pokemonsViewModel: PokemonsViewModel by sharedViewModel()
    private var _binding: FragmentFavoritesBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFavoritesBinding.inflate(inflater, container, false)
        val root: View = binding.root

        pokemonsViewModel.favoritesPokemon.observe(viewLifecycleOwner) { pokemons ->
            with(binding) {
                noPokemons.isVisible = pokemons.isEmpty()
                initRecyclerView(pokemons)
            }
        }
        pokemonsViewModel.getFavoritesPokemons()

        return root
    }

    private fun initRecyclerView(pokemons: List<Pokemon>) {
        with(binding) {
            favoritesPokemonsRecyclerview.layoutManager = GridLayoutManager(context, 2)
            favoritesPokemonsRecyclerview.adapter = FavoritePokemonsAdapter(pokemons, ::onClickDetails)
        }
    }

    private fun onClickDetails(pokemon: Pokemon) {
        val action = FavoritesFragmentDirections.actionDetailsFragmentToDetailsFragment(
            pokemon.url,
            pokemon.name,
            pokemon.photoUrl
        )
        findNavController().navigate(action)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}