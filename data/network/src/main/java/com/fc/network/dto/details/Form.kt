package com.fc.network.dto.details

data class Form(
    val name: String,
    val url: String
)