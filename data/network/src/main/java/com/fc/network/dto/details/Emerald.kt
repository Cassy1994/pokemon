package com.fc.network.dto.details

data class Emerald(
    val front_default: String,
    val front_shiny: String
)