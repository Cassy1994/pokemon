package com.fc.network.dto.details

data class MoveX(
    val name: String,
    val url: String
)