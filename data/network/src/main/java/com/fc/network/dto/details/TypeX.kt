package com.fc.network.dto.details

data class TypeX(
    val name: String,
    val url: String
)