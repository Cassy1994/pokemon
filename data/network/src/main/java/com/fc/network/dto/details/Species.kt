package com.fc.network.dto.details

data class Species(
    val name: String,
    val url: String
)