package com.fc.network.dto.details

data class Version(
    val name: String,
    val url: String
)