package com.fc.network

import com.fc.domain.utils.Const.LIMIT
import com.fc.network.dto.PokemonsResponseDto
import com.fc.network.dto.details.PokemonDetailsDto
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface ApiService {

    @GET("pokemon?limit=$LIMIT")
    suspend fun getPokemons(@Query("offset") offset: Int): Response<PokemonsResponseDto>

    @GET
    suspend fun getPokemonDetails(@Url url: String): Response<PokemonDetailsDto>
}
