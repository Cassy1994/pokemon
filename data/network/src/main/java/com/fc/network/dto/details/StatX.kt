package com.fc.network.dto.details

data class StatX(
    val name: String,
    val url: String
)