package com.fc.network.dto.details

data class GameIndice(
    val game_index: Int,
    val version: Version
)