package com.fc.network.dto.details

data class MoveLearnMethod(
    val name: String,
    val url: String
)