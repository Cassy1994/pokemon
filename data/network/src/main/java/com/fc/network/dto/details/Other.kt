package com.fc.network.dto.details

data class Other(
    val dream_world: DreamWorld,
    val home: Home,
    val officialArtwork: OfficialArtwork
)