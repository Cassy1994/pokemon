package com.fc.network.dto.details

data class GenerationIii(
    val emerald: Emerald,
    val fireredLeafgreen: FireredLeafgreen,
    val rubySapphire: RubySapphire
)