package com.fc.network.dto.details

data class OfficialArtwork(
    val front_default: String
)