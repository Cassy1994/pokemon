package com.fc.network.dto.details

data class GenerationIi(
    val crystal: Crystal,
    val gold: Gold,
    val silver: Silver
)