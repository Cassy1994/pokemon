package com.fc.network.dto.details

data class VersionGroup(
    val name: String,
    val url: String
)