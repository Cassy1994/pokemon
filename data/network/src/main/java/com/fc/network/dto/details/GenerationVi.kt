package com.fc.network.dto.details

data class GenerationVi(
    val omegarubyAlphasapphire: OmegarubyAlphasapphire,
    val xy: XY
)