package com.fc.network.dto

data class PokemonDto(
    val name: String,
    val url: String
)