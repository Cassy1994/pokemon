package com.fc.network.utils

import java.io.IOException

sealed class ApiException(msg: String) : IOException(msg) {
    class NoInternetException(message: String = "No internet connection") : IOException(message)
    class NotFound(message: String = "Endpoint not founded") : IOException(message)
    class GenericError(msg: String = "Ops, something went wrong") : Error(msg)
}