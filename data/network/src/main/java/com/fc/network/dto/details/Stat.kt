package com.fc.network.dto.details

data class Stat(
    val base_stat: Int,
    val effort: Int,
    val stat: StatX
)