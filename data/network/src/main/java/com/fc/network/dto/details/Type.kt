package com.fc.network.dto.details

data class Type(
    val slot: Int,
    val type: TypeX
)