package com.fc.network.dto.details

data class GenerationI(
    val redBlue: RedBlue,
    val yellow: Yellow
)