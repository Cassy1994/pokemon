package com.fc.network.dto.details

data class AbilityXDto(
    val name: String,
    val url: String
)