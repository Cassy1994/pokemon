package com.fc.network.dto

data class PokemonsResponseDto(
    val count: Int,
    val next: String,
    val previous: String,
    val results: List<PokemonDto>
)