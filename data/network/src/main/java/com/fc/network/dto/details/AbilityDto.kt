package com.fc.network.dto.details

data class AbilityDto(
    val ability: AbilityXDto,
    val is_hidden: Boolean,
    val slot: Int
)