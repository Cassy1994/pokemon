package com.fc.network.dto.details

data class GenerationVii(
    val icons: Icons,
    val ultraSunUltraMoon: UltraSunUltraMoon
)