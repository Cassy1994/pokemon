package com.fc.repository

import com.fc.cache.model.FavoritePokemonEntity
import com.fc.cache.model.PokemonEntity
import com.fc.domain.models.Pokemon
import com.fc.domain.models.details.Ability
import com.fc.domain.models.details.AbilityX
import com.fc.domain.models.details.PokemonDetails
import com.fc.network.dto.PokemonDto
import com.fc.network.dto.details.AbilityDto
import com.fc.network.dto.details.PokemonDetailsDto

internal fun PokemonDto.toEntity(): PokemonEntity {
    return PokemonEntity(
        name = name,
        url = url
    )
}

internal fun PokemonEntity.toDomain(isFavorite: Boolean = false): Pokemon {
    return Pokemon(
        name = name,
        url = url,
        isFavorite = isFavorite
    )
}

internal fun Pokemon.toFavoritePokemon(): FavoritePokemonEntity {
    return FavoritePokemonEntity(
        name = name,
        url = url
    )
}

internal fun FavoritePokemonEntity.toDomain(): Pokemon {
    return Pokemon(
        name = name,
        url = url
    )
}

internal fun PokemonDetailsDto.toDomain(): PokemonDetails {
    return PokemonDetails(
        abilities = abilities.map { it.toDomain() }
    )
}

internal fun AbilityDto.toDomain(): Ability {
    return Ability(
        isHidden = is_hidden,
        slot = slot,
        ability = AbilityX(name = ability.name)
    )
}