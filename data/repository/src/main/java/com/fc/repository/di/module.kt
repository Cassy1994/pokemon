package com.fc.repository.di

import com.fc.domain.reporistories.PokemonsRepository
import com.fc.repository.pokemonRepository.PokemonRepositoryImpl
import org.koin.dsl.module

val pokemonRepositoryModule = module {

    single<PokemonsRepository> {
        PokemonRepositoryImpl(
            apiService = get(),
            pokemonDao = get()
        )
    }
}