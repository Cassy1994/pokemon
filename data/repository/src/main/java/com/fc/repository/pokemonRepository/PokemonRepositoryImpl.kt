package com.fc.repository.pokemonRepository

import com.fc.cache.dao.PokemonDao
import com.fc.cache.model.PokemonEntity
import com.fc.domain.models.Error
import com.fc.domain.models.Pokemon
import com.fc.domain.models.Resource
import com.fc.domain.models.details.PokemonDetails
import com.fc.domain.reporistories.PokemonsRepository
import com.fc.network.ApiService
import com.fc.network.utils.SafeApiRequest
import com.fc.repository.toDomain
import com.fc.repository.toEntity
import com.fc.repository.toFavoritePokemon
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.net.UnknownHostException

class PokemonRepositoryImpl(
    private val apiService: ApiService,
    private val pokemonDao: PokemonDao,
): PokemonsRepository, SafeApiRequest() {

    override suspend fun getPokemons(offset: Int): Flow<Resource<List<Pokemon>>> = flow {
        try {
            val result = safeApiRequest { apiService.getPokemons(offset) }
            val pokemonToCache = result.results.map { it.toEntity() }
            pokemonDao.insert(pokemonToCache)
        } catch (e: UnknownHostException) {
            emit(Resource.Error(Error.NoInternetConnection()))
        } catch (e: Exception) {
            emit(Resource.Error(Error.GenericError()))
        } finally {
            emit(Resource.Success(getPokemonsFromCache(offset)))
        }
    }

    override suspend fun searchPokemons(query: CharSequence): Resource<List<Pokemon>> {
        val result = pokemonDao.searchPokemons(query.toString())
        val pokemons = pokemonItemIsFavoriteFromList(result)
        return Resource.Success(pokemons)
    }

    override suspend fun getFavoritePokemons(): Resource<List<Pokemon>> {
        val favoritePokemons = pokemonDao.getFavoritePokemons()
        val pokemons = favoritePokemons.map { it.toDomain() }
        return Resource.Success(pokemons)
    }

    override suspend fun addRemoveFavoritePokemon(pokemon: Pokemon): Resource<Pokemon> {
        if (pokemon.isFavorite) {
            pokemonDao.insert(pokemon.toFavoritePokemon())
        } else {
            pokemonDao.deleteFavoritePokemon(pokemon.name)
        }
        return Resource.Success(pokemon)
    }

    override suspend fun getPokemonDetails(url: String): Resource<PokemonDetails> {
        return try {
            val result = safeApiRequest { apiService.getPokemonDetails(url) }
            Resource.Success(result.toDomain())
        } catch (e: UnknownHostException) {
            Resource.Error(e)
        } catch (e: Exception) {
            Resource.Error(e)
        }
    }

    private suspend fun pokemonItemIsFavoriteFromList(pokemons: List<PokemonEntity>): List<Pokemon> {
        val favoritePokemons = pokemonDao.getFavoritePokemons()
        val pokemonResponse = pokemons.map { pokemonDto ->
            val founded = favoritePokemons.find { it.name == pokemonDto.name }
            pokemonDto.toDomain(founded != null)
        }
        return pokemonResponse
    }

    private suspend fun getPokemonsFromCache(offset: Int): List<Pokemon> {
        val pokemons = pokemonDao.getPokemons(offset)
        return pokemonItemIsFavoriteFromList(pokemons)
    }
}