package com.fc.cache.model

abstract class BaseModel(
    open val name: String,
    open val url: String
)