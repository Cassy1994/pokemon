package com.fc.cache.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Pokemon")
data class PokemonEntity(
    @PrimaryKey
    override val name: String,
    override val url: String,
): BaseModel(
    name = name,
    url = url
)