package com.fc.cache.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.fc.cache.model.FavoritePokemonEntity
import com.fc.cache.model.PokemonEntity

@Dao
interface PokemonDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(pokemons: List<PokemonEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(pokemon: FavoritePokemonEntity)

    @Query("DELETE FROM `FavoritePokemonEntity` WHERE name = :name")
    suspend fun deleteFavoritePokemon(name: String)

    @Query("SELECT * FROM `Pokemon` LIMIT 50 OFFSET :offset")
    suspend fun getPokemons(offset: Int): List<PokemonEntity>

    @Query("SELECT * FROM `FavoritePokemonEntity`")
    suspend fun getFavoritePokemons(): List<FavoritePokemonEntity>

    @Query("SELECT * FROM `Pokemon` WHERE name LIKE :query || '%'")
    suspend fun searchPokemons(query: String): List<PokemonEntity>
}