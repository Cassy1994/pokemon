package com.fc.cache.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "FavoritePokemonEntity")
data class FavoritePokemonEntity(
    @PrimaryKey
    override val name: String,
    override val url: String
): BaseModel(
    name = name,
    url = url
)