package com.fc.cache

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.fc.cache.dao.PokemonDao
import com.fc.cache.model.FavoritePokemonEntity
import com.fc.cache.model.PokemonEntity

@Database(entities = [PokemonEntity::class, FavoritePokemonEntity::class], version = 1, exportSchema = true)
abstract class AppDatabase : RoomDatabase() {

    abstract fun pokemonDao(): PokemonDao

    companion object {

        fun buildDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
                .build()
        }
    }
}

private const val DATABASE_NAME = "pokemon-db"