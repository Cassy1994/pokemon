package com.fc.cache.di

import com.fc.cache.AppDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val cacheModule = module{

    single {
        AppDatabase.buildDatabase(androidContext()).pokemonDao()
    }
}