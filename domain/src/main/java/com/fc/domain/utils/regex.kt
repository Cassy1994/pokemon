package com.fc.domain.utils

internal fun getPokemonIdFromUrl(url: String): Int {
    val pattern = Regex("(\\d+)(?!.*\\d)")
    val match = pattern.find(url)
    return match?.value?.toInt() ?: 0
}