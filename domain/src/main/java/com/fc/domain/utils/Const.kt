package com.fc.domain.utils

object Const {
    const val BASE_URL = "https://pokeapi.co/api/v2/"
    const val LIMIT = 50
    const val START_OFFSET = 0
}