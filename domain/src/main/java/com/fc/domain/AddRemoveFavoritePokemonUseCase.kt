package com.fc.domain

import com.fc.domain.models.Pokemon
import com.fc.domain.models.Resource
import com.fc.domain.reporistories.PokemonsRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class AddRemoveFavoritePokemonUseCase(
    private val pokemonsRepository: PokemonsRepository
) {
    operator fun invoke(pokemon: Pokemon): Flow<Resource<Pokemon>> = flow {
        val response = pokemonsRepository.addRemoveFavoritePokemon(pokemon)
        emit(response)
    }
}