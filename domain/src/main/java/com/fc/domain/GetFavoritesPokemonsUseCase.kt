package com.fc.domain

import com.fc.domain.models.Pokemon
import com.fc.domain.models.Resource
import com.fc.domain.reporistories.PokemonsRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class GetFavoritesPokemonsUseCase(
    private val pokemonsRepository: PokemonsRepository
) {
    suspend operator fun invoke(): Flow<Resource<List<Pokemon>>>  = flow {
        val response = pokemonsRepository.getFavoritePokemons()
        emit(response)
    }
}