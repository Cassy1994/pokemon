package com.fc.domain

import com.fc.domain.models.Resource
import com.fc.domain.models.details.PokemonDetails
import com.fc.domain.reporistories.PokemonsRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class GetPokemonDetailsUseCase(
    private val pokemonsRepository: PokemonsRepository
) {
    suspend operator fun invoke(url: String): Flow<Resource<PokemonDetails>> = flow {
        val response = pokemonsRepository.getPokemonDetails(url)
        emit(response)
    }
}
