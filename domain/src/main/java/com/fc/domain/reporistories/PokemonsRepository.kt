package com.fc.domain.reporistories

import com.fc.domain.models.Pokemon
import com.fc.domain.models.Resource
import com.fc.domain.models.details.PokemonDetails
import kotlinx.coroutines.flow.Flow

interface PokemonsRepository {
    suspend fun getPokemons(offset: Int): Flow<Resource<List<Pokemon>>>
    suspend fun searchPokemons(query: CharSequence): Resource<List<Pokemon>>
    suspend fun getFavoritePokemons(): Resource<List<Pokemon>>
    suspend fun addRemoveFavoritePokemon(pokemon: Pokemon): Resource<Pokemon>
    suspend fun getPokemonDetails(url: String): Resource<PokemonDetails>
}
