package com.fc.domain

import com.fc.domain.models.Pokemon
import com.fc.domain.models.Resource
import com.fc.domain.reporistories.PokemonsRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class SearchPokemonsUseCase(
    private val pokemonsRepository: PokemonsRepository
) {
    operator fun invoke(query: CharSequence): Flow<Resource<List<Pokemon>>> = flow {
        val response = pokemonsRepository.searchPokemons(query)
        emit(response)
    }
}