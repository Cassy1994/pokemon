package com.fc.domain.models

data class PokemonsResponse(
    val next: String,
    val previous: String,
    val results: List<Pokemon>
)