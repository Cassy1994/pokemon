package com.fc.domain.models

import com.fc.domain.utils.getPokemonIdFromUrl

data class Pokemon(
    val name: String,
    val url: String,
    val isFavorite: Boolean = false
) {
    private fun getId() = getPokemonIdFromUrl(url)
    val photoUrl =
        "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${getId()}.png"
}