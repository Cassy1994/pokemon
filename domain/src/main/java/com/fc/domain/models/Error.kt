package com.fc.domain.models

sealed class Error(msg: String) : Throwable(msg) {
    class NoInternetConnection(msg: String = "No internet connection") : Error(msg)
    class GenericError(msg: String = "Ops, something went wrong") : Error(msg)
}