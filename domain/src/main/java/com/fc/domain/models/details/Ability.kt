package com.fc.domain.models.details

data class Ability(
    val ability: AbilityX,
    val isHidden: Boolean,
    val slot: Int
)