package com.fc.domain.models.details

class PokemonDetails(
    val abilities: List<Ability>
)