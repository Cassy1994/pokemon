package com.fc.domain

import com.fc.domain.models.Pokemon
import com.fc.domain.models.Resource
import com.fc.domain.reporistories.PokemonsRepository
import kotlinx.coroutines.flow.Flow

class GetPokemonsUseCase(
    private val pokemonsRepository: PokemonsRepository
) {
    suspend operator fun invoke(offset: Int): Flow<Resource<List<Pokemon>>> =
        pokemonsRepository.getPokemons(offset)
}